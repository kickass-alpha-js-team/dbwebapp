import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { TestUtils } from './utils/test.utils';
import { AuthModule } from '../src/modules/auth/auth.module';
import { AuthorModule } from '../src/modules/authors.module';
import { BooksModule } from '../src/modules/books.module';
import { GenresModule } from '../src/modules/genres.module';
import { UserModule } from '../src/modules/user.module';
import { ScrapeModule } from '../src/modules/scrape.module';
import * as request from 'supertest';
import { ConfigModule } from '../src/config/config.module';
import { DatabaseModule } from './utils/database/database.module';
import { CoreModule } from '../src/modules/core/core.module';

describe('GenresController (e2e)', async () => {
    let app: INestApplication;
    let testUtils: TestUtils;

    beforeEach(async () => {
        const moduleFixture = await Test.createTestingModule({
            imports: [
                AuthModule,
                AuthorModule,
                BooksModule,
                GenresModule,
                ConfigModule,
                DatabaseModule,
                CoreModule,
                ScrapeModule,
                UserModule,
            ],
            providers: [
                TestUtils,
            ],
        })
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
        testUtils = app.get<TestUtils>(TestUtils);
        await testUtils.reloadFixtures();
    });

    afterEach(async done => {
        await testUtils.closeDbConnection();
        done();
    });

    it('/books/all (GET)', () => {
        return request(app.getHttpServer())
        .get('/books/all')
        .expect(200)
        .expect(`[{"id":"483957fc-c232-4325-9835-c544ccbde7d1","title":"Short Stories from Hogwarts of Heroism, Hardship and Dangerous Hobbies","pages":71,"issued":"September 6th 2016","coverUrl":"https://images.gr-assets.com/books/1471436733l/31538635.jpg","description":"test","ISBN":"1781106282"}]`);
    });
});
