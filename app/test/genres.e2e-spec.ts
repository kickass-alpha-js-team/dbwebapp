import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { TestUtils } from './utils/test.utils';
import { AuthModule } from '../src/modules/auth/auth.module';
import { AuthorModule } from '../src/modules/authors.module';
import { BooksModule } from '../src/modules/books.module';
import { GenresModule } from '../src/modules/genres.module';
import { UserModule } from '../src/modules/user.module';
import { ScrapeModule } from '../src/modules/scrape.module';
import * as request from 'supertest';
import { ConfigModule } from '../src/config/config.module';
import { DatabaseModule } from './utils/database/database.module';
import { CoreModule } from '../src/modules/core/core.module';

describe('GenresController (e2e)', async () => {
    let app: INestApplication;
    let testUtils: TestUtils;

    beforeEach(async () => {
        const moduleFixture = await Test.createTestingModule({
            imports: [
                AuthModule,
                AuthorModule,
                BooksModule,
                GenresModule,
                ConfigModule,
                DatabaseModule,
                CoreModule,
                ScrapeModule,
                UserModule,
            ],
            providers: [
                TestUtils,
            ],
        })
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
        testUtils = app.get<TestUtils>(TestUtils);
        await testUtils.reloadFixtures();
    });

    afterEach(async done => {
        await testUtils.closeDbConnection();
        done();
    });

    it('/genres (GET)', () => {
        return request(app.getHttpServer())
        .get('/genres/all')
        .expect(200)
        .expect('[{"id":1,"genreName":"Fiction"}]');
    });
});
