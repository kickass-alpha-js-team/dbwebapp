import { AuthorsService } from './services/authors.service';
import { Module } from '@nestjs/common';
import { AuthorsController } from './controllers/authors.controller';
import { AuthorModule } from './modules/authors.module';
import { ConfigModule } from './config/config.module';
import { DatabaseModule } from './database/database.module';
import { UserModule } from './modules/user.module';
import { AuthModule } from './modules/auth/auth.module';
import { BooksModule } from './modules/books.module';
import { ScrapeModule } from './modules/scrape.module';
import { GenresModule } from './modules/genres.module';

@Module({
  imports: [
    AuthModule,
    AuthorModule,
    BooksModule,
    GenresModule,
    ConfigModule,
    DatabaseModule,
    ScrapeModule,
    UserModule,
  ],
})
export class AppModule { }
