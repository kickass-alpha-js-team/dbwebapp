import { Controller, Get, Post, Body, UseGuards, ValidationPipe } from '@nestjs/common';
import { ScrapeService } from '../services/scrape.service';
import { UrlDTO } from '../models/scraper/book-url.dto';
import { BookScrapeSearchDTO } from '../models/scraper/book-search.dto';
import { AuthGuard } from '@nestjs/passport';
import { AdminGuard } from '../common/guards/admin.guard';
import { BooksService } from '../services/books.service';
import { AuthorsService } from '../services/authors.service';

@Controller('scrape')
export class ScrapeController {
  constructor(
    private readonly scrapeService: ScrapeService,
    private readonly booksService: BooksService,
    private readonly authorsService: AuthorsService,
  ) { }

  @UseGuards(AuthGuard(), AdminGuard)
  @Post('/search/')
  async scrapeSearch(@Body() bssDTO: BookScrapeSearchDTO) {
    const booksArr = await this.scrapeService.bookSearch(bssDTO.title);

    return booksArr;
  }

   @UseGuards(AuthGuard(), AdminGuard)
  @Post('/search/add')
  async scrapeSearchAdd(@Body() bssDTO: BookScrapeSearchDTO) {
    const booksArr = await this.scrapeService.bookSearch(bssDTO.title);
    const bookUrl = booksArr[+(bssDTO.id) - 1].link;
    const bookToAdd = await this.scrapeService.addBook(bookUrl);

    // return bookToAdd
    return this.booksService.addBook(bookToAdd);
  }

  @UseGuards(AuthGuard(), AdminGuard)
  @Post('/book')
  async scrapeAdd(@Body(new ValidationPipe({
    transform: true,
    whitelist: true,
  })) bookUrlDTO: UrlDTO) {
    const bookToAdd = await this.scrapeService.addBook(bookUrlDTO.url);

    // return bookToAdd;
    return this.booksService.addBook(bookToAdd);
  }

  @UseGuards(AuthGuard(), AdminGuard)
  @Post('/author')
  async author(@Body(new ValidationPipe({
    transform: true,
    whitelist: true,
  })) link: UrlDTO) {
    const authorToAdd = await this.scrapeService.addAuthor(link.url);

    // return authorToAdd;
    return this.authorsService.addAuthor(authorToAdd);
  }
}
