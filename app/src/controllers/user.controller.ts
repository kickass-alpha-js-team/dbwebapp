import { Controller, UseFilters, Post, UseInterceptors,
   FileInterceptor, Body, Request , ValidationPipe, UploadedFile,
    BadRequestException, UseGuards, Get, ParseIntPipe, Query, Delete } from '@nestjs/common';

import { AuthService } from '../services/auth/auth.service';
import { UserService } from '../services/user.service';
import { UserDetailsService } from '../services/user-details.service';
import { BadRequestExceptionFilter } from '../common/filters/badrequest-exception.filter';
import { FileService } from '../services/file.service';
import { UserRegisterDTO } from '../models/user/user-register.dto';
import { join } from 'path';
import { unlink } from 'fs';
import { UserLoginDTO } from '../models/user/user-login.dto';
import { AuthGuard } from '@nestjs/passport';
import { AdminGuard } from '../common/guards/admin.guard';
import { User } from '../database/entities/user.entity';
import { UserDetailsDTO } from '../models/user/user-details.dto';
import { UserDetails } from '../database/entities/user-details.entity';
import { FavoriteBooksDTO } from '../models/favorite-books.dto';

@Controller('user')
export class UserController {

  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,

    private readonly userDetailsService: UserDetailsService,
  ) { }

  @UseFilters(BadRequestExceptionFilter)
  @Post('register')
  @UseInterceptors(FileInterceptor('avatar', {
    limits: FileService.fileLimit(1, 2 * 1024 * 1024),
    storage: FileService.storage(['public', 'images']),
    fileFilter: (req, file, cb) => FileService.fileFilter(req, file, cb, '.png', '.jpg'),
  }))
  async register(
    @Body(new ValidationPipe({
      transform: true,
      whitelist: true,
    }))
    user: UserRegisterDTO,

    @UploadedFile()
    file,
  ): Promise<string> {
    const folder = join('.', 'public', 'uploads');
    if (!file) {
      user.avatarUrl = join(folder, 'default.png');
    } else {
      user.avatarUrl = join(folder, file.filename);
    }

    try {
      return await this.userService.registerUser(user);

    } catch (error) {
      await new Promise((resolve, reject) => {

        if (file) {
          unlink(join('.', file.path), (err) => {
            if (err) {
              reject(error.message);
            }
            resolve();
          });
        }

        resolve();
      });

      throw error;
    }
  }

  @UseFilters(BadRequestExceptionFilter)
  @Post('login')
  async sign(@Body(new ValidationPipe({
    transform: true,
    whitelist: true,
  })) user: UserLoginDTO): Promise<string> {
    const token = await this.authService.signIn(user);
    if (!token) {
      throw new BadRequestException('Wrong credentials!');
    }

    return token;
  }

  @UseGuards(AuthGuard(), AdminGuard)
  @UseFilters(BadRequestExceptionFilter)
  @Get()
  async getUserById(@Query('id', new ParseIntPipe()) id: number): Promise<User> {
    const userFound: User = await this.userService.getUserById(id);

    return userFound;
  }

  @UseGuards(AuthGuard())
  @Post(':username/details')
  async updateUserDetails(@Request() req, @Body(new ValidationPipe({
    transform: true,
    whitelist: true,
  })) details: UserDetailsDTO): Promise<UserDetails> {
    const detailsUpdated: UserDetails = await this.userDetailsService.updateDetails(req, details);

    return detailsUpdated;
  }

  @UseGuards(AuthGuard())
  @UseFilters(BadRequestExceptionFilter)
  @Post(':username/details/favorite-books')
  async addFavoriteBooks(@Request() req, @Body(new ValidationPipe({
    transform: true,
    whitelist: true,
  })) favoriteBooks: FavoriteBooksDTO): Promise<string> {
    const successMessage: string = await this.userDetailsService.addFavoriteBooks(req, favoriteBooks);

    return successMessage;
  }

  @UseGuards(AuthGuard())
  @Get(':username/details')
  async getUserDetails(@Request() req): Promise<User> {
    const userFound: User = await this.userDetailsService.getUserDetails(req);

    return userFound;
  }

  @UseGuards(AuthGuard())
  @UseFilters(BadRequestExceptionFilter)
  @Delete(':username/details/favorite-books')
  async removeFavoriteBooks(@Request() req,  @Body(new ValidationPipe({
    transform: true,
    whitelist: true,
  })) booksToDelete: FavoriteBooksDTO): Promise<string> {
    const bookDeletedMessage: string = await this.userDetailsService.removeFromUserFavoriteBooks(req, booksToDelete);

    return bookDeletedMessage;
  }
}
