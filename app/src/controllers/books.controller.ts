import { Controller, Get, Post, Body, Param, UseGuards, Put, ValidationPipe, UseFilters } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BooksService } from '../services/books.service';
import { AdminGuard } from '../common/guards/admin.guard';
import { BadRequestExceptionFilter } from '../common/filters/badrequest-exception.filter';
import { BookDTO } from '../models/book.dto';
import { Book } from '../database/entities/book.entity';
import { UnauthorizedExceptionFilter } from '../common/filters/unauthorized-exception.filter';

@Controller('books')
export class BooksController {
    constructor(private readonly booksService: BooksService) { }

    @UseGuards(AuthGuard(), AdminGuard)
    @UseFilters(BadRequestExceptionFilter, UnauthorizedExceptionFilter)
    @Post()
    async add(@Body(new ValidationPipe({
        transform: true,
        whitelist: true,
    })) book: BookDTO): Promise<string> {
        return await this.booksService.addBook(book);
    }

    @UseGuards(AuthGuard(), AdminGuard)
    @UseFilters(BadRequestExceptionFilter)
    @Put(':title')
    updateBookByName(@Param('title') title, @Body(new ValidationPipe({
        whitelist: true,
        transform: true,
    })) book: BookDTO): Promise<string> {
        return this.booksService.updateBookByTitle(title, book);
    }

    @Get('all')
    getAll(): Promise<Book[]> {
        return this.booksService.getAll();
    }

    @UseFilters(BadRequestExceptionFilter)
    @Get(':title')
    findByTitle(@Param('title') title): Promise<Book> {
        return this.booksService.getBookByTitle(title);
    }

    @UseFilters(BadRequestExceptionFilter)
    @Get('/letter/:letter')
    getBooksByFirstLetters(@Param('letter') letter): Promise<Book[]> {
        return this.booksService.getBooksByFirstLetters(letter);
    }
}
