import { Controller, Get, Post, Body, Param, UseGuards, Put, ValidationPipe, UseFilters } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthorsService } from '../services/authors.service';
import { Author } from '../database/entities/author.entity';
import { AdminGuard } from '../common/guards/admin.guard';
import { AuthorDTO } from '../models/author.dto';
import { BadRequestExceptionFilter } from '../common/filters/badrequest-exception.filter';

@Controller('authors')
export class AuthorsController {
    constructor(
        private readonly authorsService: AuthorsService,
    ) { }

    @UseGuards(AuthGuard(), AdminGuard)
    @UseFilters(BadRequestExceptionFilter)
    @Post()
    async addAuthor(@Body(new ValidationPipe({
        whitelist: true,
        transform: true,
    })) author: AuthorDTO): Promise<string> {
        return await this.authorsService.addAuthor(author);
    }

    @UseGuards(AuthGuard(), AdminGuard)
    @UseFilters(BadRequestExceptionFilter)
    @Put(':fullName')
    updateAuthorByName(@Param('fullName') fullName, @Body(new ValidationPipe({
        whitelist: true,
        transform: true,
    })) author: AuthorDTO): Promise<string> {
        return this.authorsService.updateAuthorByName(fullName, author);
    }

    @Get('all')
    getAll(): Promise<Author[]> {
        return this.authorsService.getAll();
    }

    @UseFilters(BadRequestExceptionFilter)
    @Get(':fullName')
    findByName(@Param('fullName') fullName): Promise<Author> {
        return this.authorsService.getAuthorByName(fullName);
    }

    @UseFilters(BadRequestExceptionFilter)
    @Get('/letter/:letter')
    getAuthorsByFirstLetters(@Param('letter') letter): Promise<Author[]> {
        return this.authorsService.getAuthorsByFirstLetters(letter);
    }

}
