import { Genre } from './../database/entities/genre.entity';
import { Controller, Get, Post, Body, UseGuards, Put, Param, ValidationPipe, UseFilters } from '@nestjs/common';
import { GenresService } from '../services/genres.services';
import { AdminGuard } from '../common/guards/admin.guard';
import { AuthGuard } from '@nestjs/passport';
import { GenreDTO } from '../models/genre.dto';
import { BadRequestExceptionFilter } from '../common/filters/badrequest-exception.filter';

@Controller('genres')
export class GenresController {
    constructor(
        private readonly genresService: GenresService,
    ) { }

    @UseGuards(AuthGuard(), AdminGuard)
    @UseFilters(BadRequestExceptionFilter)
    @Post()
    async addGenre(@Body(new ValidationPipe({
        whitelist: true,
        transform: true,
    })) genre: GenreDTO): Promise<string> {
        return await this.genresService.addGenre(genre);
    }

    @UseGuards(AuthGuard(), AdminGuard)
    @UseFilters(BadRequestExceptionFilter)
    @Put(':genreName')
    async updateGenreByName(@Param('genreName') genreName, @Body(new ValidationPipe({
        whitelist: true,
        transform: true,
    })) genre: GenreDTO): Promise<string> {
        return this.genresService.updateGenreByName(genreName, genre);
    }

    @Get('all')
    getAll(): Promise<Genre[]> {
        return this.genresService.getAll();
    }

}
