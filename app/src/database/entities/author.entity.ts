import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable } from 'typeorm';
import { Book } from './book.entity';
import { Genre } from './genre.entity';

@Entity('authors')
export class Author {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        name: 'fullName',
        type: 'nvarchar',
        length: 170,
        unique: true,
    })
    fullName: string;

    @Column()
    born: string;

    @Column({
        nullable: true,
    })
    died: string;

    @Column('mediumtext')
    biography: string;

    @Column({
        type: 'varchar',
        length: 2083,
    })
    pictureUrl: string;

    @ManyToMany(type => Genre, genre => genre.authors/* , { cascade: ['remove'] } */)
    @JoinTable()
    genres: Genre[];

    @ManyToMany(type => Book, book => book.authors/* , {  cascade: ['remove'] } */)
    books: Book[];
}
