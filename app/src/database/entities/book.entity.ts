import { Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, ManyToMany, JoinTable } from 'typeorm';
import { Genre } from './genre.entity';
import { Author } from './author.entity';
import { UserDetails } from './user-details.entity';

@Entity('books')
export class Book {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: 'nvarchar',
        length: 200,
        unique: true,
    })
    title: string;

    @Column()
    pages: number;

    @Column()
    issued: string;

    @Column({
        type: 'varchar',
        length: 2083,
    })
    coverUrl: string;

    @Column('mediumtext')
    description: string;

    @Column({
        type: 'varchar',
        length: 45,
    })
    ISBN: string;

    @ManyToMany(type => Genre, genre => genre.books)
    @JoinTable()
    genres: Genre[];

    @ManyToMany(type => Author, author => author.books)
    @JoinTable()
    authors: Author[];

    @ManyToMany(type => UserDetails, details => details.books)
    details: UserDetails[];
}
