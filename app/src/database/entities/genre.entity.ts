import { Entity, PrimaryGeneratedColumn, Column, ManyToMany } from 'typeorm';
import { Book } from './book.entity';
import { Author } from './author.entity';

@Entity('genres')
export class Genre {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        name: 'genreName',
        type: 'nvarchar',
        length: 30,
        unique: true,
        default: '',
    })
    genreName: string;

    @ManyToMany(type => Author, author => author.genres)
    authors: Author[];

    @ManyToMany(type => Book, book => book.genres)
    books: Book[];
}
