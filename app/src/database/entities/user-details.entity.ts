import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToMany, JoinTable, OneToOne } from 'typeorm';
import { Book } from './book.entity';
import { User } from './user.entity';

@Entity('details')
export class UserDetails {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'nvarchar',
        length: 45,
        nullable: true,
    })
    firstName: string;

    @Column({
        type: 'nvarchar',
        length: 45,
        nullable: true,
    })
    lastName: string;

    @Column({
        nullable: true,
    })
    age: number;

    @ManyToMany(type => Book, book => book.details, {
        eager: true,
    })
    @JoinTable()
    books: Book[];

    @OneToOne(type => User, user => user.details)
    user: User;

}
