import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, CreateDateColumn } from 'typeorm';
import { IsEmail } from 'class-validator';
import { UserDetails } from './user-details.entity';

@Entity('users')
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'nvarchar',
        length: 45,
        unique: true,
    })
    username: string;

    @CreateDateColumn()
    memberSince: Date;

    @Column({
        unique: true,
    })
    @IsEmail()
    email: string;

    @Column({
        type: 'nvarchar',
        length: 76,
    })
    password: string;

    @Column({ default: false })
    isAdmin: boolean;

    @Column({
        type: 'varchar',
        length: 2083,
    })
    avatarUrl: string;

    @OneToOne(type => UserDetails, details => details.user, {
        eager: true,
    })
    @JoinColumn()
    details: UserDetails;
}
