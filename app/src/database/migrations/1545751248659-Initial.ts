import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial1545751248659 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `genres` (`id` int NOT NULL AUTO_INCREMENT, `genreName` varchar(30) NOT NULL DEFAULT '', UNIQUE INDEX `IDX_8d492be5f49e0233f3567b5986` (`genreName`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users` (`id` int NOT NULL AUTO_INCREMENT, `username` varchar(45) NOT NULL, `email` varchar(255) NOT NULL, `password` varchar(76) NOT NULL, `isAdmin` tinyint NOT NULL DEFAULT 0, `avatarUrl` varchar(2083) NOT NULL, `detailsId` int NULL, UNIQUE INDEX `IDX_fe0bb3f6520ee0469504521e71` (`username`), UNIQUE INDEX `REL_a8687924ae4d52f05db87f3352` (`detailsId`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `details` (`id` int NOT NULL AUTO_INCREMENT, `firstName` varchar(45) NOT NULL, `lastName` varchar(45) NULL, `age` int NOT NULL, `memberSince` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `books` (`id` varchar(255) NOT NULL, `title` varchar(200) NOT NULL, `pages` int NOT NULL, `issued` varchar(255) NOT NULL, `coverUrl` varchar(2083) NOT NULL, `description` mediumtext NOT NULL, `ISBN` varchar(45) NOT NULL, UNIQUE INDEX `IDX_3cd818eaf734a9d8814843f119` (`title`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `authors` (`id` varchar(255) NOT NULL, `fullName` varchar(150) NOT NULL, `born` varchar(255) NOT NULL, `died` varchar(255) NULL, `biography` mediumtext NOT NULL, `pictureUrl` varchar(2083) NOT NULL, UNIQUE INDEX `IDX_c94116fd7c297e31fa8eb7a41f` (`fullName`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `details_books_books` (`detailsId` int NOT NULL, `booksId` varchar(255) NOT NULL, PRIMARY KEY (`detailsId`, `booksId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `books_genres_genres` (`booksId` varchar(255) NOT NULL, `genresId` int NOT NULL, PRIMARY KEY (`booksId`, `genresId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `books_authors_authors` (`booksId` varchar(255) NOT NULL, `authorsId` varchar(255) NOT NULL, PRIMARY KEY (`booksId`, `authorsId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `authors_genres_genres` (`authorsId` varchar(255) NOT NULL, `genresId` int NOT NULL, PRIMARY KEY (`authorsId`, `genresId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_a8687924ae4d52f05db87f3352f` FOREIGN KEY (`detailsId`) REFERENCES `details`(`id`)");
        await queryRunner.query("ALTER TABLE `details_books_books` ADD CONSTRAINT `FK_b7239fa3357004a814527d1f821` FOREIGN KEY (`detailsId`) REFERENCES `details`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `details_books_books` ADD CONSTRAINT `FK_955a258f2b9a93e0a1f5c710d29` FOREIGN KEY (`booksId`) REFERENCES `books`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `books_genres_genres` ADD CONSTRAINT `FK_e1c8b5fb4c9afac80b2591b0c84` FOREIGN KEY (`booksId`) REFERENCES `books`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `books_genres_genres` ADD CONSTRAINT `FK_8d2218df7344c443d9ded154921` FOREIGN KEY (`genresId`) REFERENCES `genres`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `books_authors_authors` ADD CONSTRAINT `FK_25a2cff0aa5b6d28dfbfd1f40ca` FOREIGN KEY (`booksId`) REFERENCES `books`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `books_authors_authors` ADD CONSTRAINT `FK_5869907ade47c42570389388d25` FOREIGN KEY (`authorsId`) REFERENCES `authors`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `authors_genres_genres` ADD CONSTRAINT `FK_8307e825982327fa3f0826b1831` FOREIGN KEY (`authorsId`) REFERENCES `authors`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `authors_genres_genres` ADD CONSTRAINT `FK_d353d6cf8b54599feee7e1aa390` FOREIGN KEY (`genresId`) REFERENCES `genres`(`id`) ON DELETE CASCADE");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `authors_genres_genres` DROP FOREIGN KEY `FK_d353d6cf8b54599feee7e1aa390`");
        await queryRunner.query("ALTER TABLE `authors_genres_genres` DROP FOREIGN KEY `FK_8307e825982327fa3f0826b1831`");
        await queryRunner.query("ALTER TABLE `books_authors_authors` DROP FOREIGN KEY `FK_5869907ade47c42570389388d25`");
        await queryRunner.query("ALTER TABLE `books_authors_authors` DROP FOREIGN KEY `FK_25a2cff0aa5b6d28dfbfd1f40ca`");
        await queryRunner.query("ALTER TABLE `books_genres_genres` DROP FOREIGN KEY `FK_8d2218df7344c443d9ded154921`");
        await queryRunner.query("ALTER TABLE `books_genres_genres` DROP FOREIGN KEY `FK_e1c8b5fb4c9afac80b2591b0c84`");
        await queryRunner.query("ALTER TABLE `details_books_books` DROP FOREIGN KEY `FK_955a258f2b9a93e0a1f5c710d29`");
        await queryRunner.query("ALTER TABLE `details_books_books` DROP FOREIGN KEY `FK_b7239fa3357004a814527d1f821`");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_a8687924ae4d52f05db87f3352f`");
        await queryRunner.query("DROP TABLE `authors_genres_genres`");
        await queryRunner.query("DROP TABLE `books_authors_authors`");
        await queryRunner.query("DROP TABLE `books_genres_genres`");
        await queryRunner.query("DROP TABLE `details_books_books`");
        await queryRunner.query("DROP INDEX `IDX_c94116fd7c297e31fa8eb7a41f` ON `authors`");
        await queryRunner.query("DROP TABLE `authors`");
        await queryRunner.query("DROP INDEX `IDX_3cd818eaf734a9d8814843f119` ON `books`");
        await queryRunner.query("DROP TABLE `books`");
        await queryRunner.query("DROP TABLE `details`");
        await queryRunner.query("DROP INDEX `REL_a8687924ae4d52f05db87f3352` ON `users`");
        await queryRunner.query("DROP INDEX `IDX_fe0bb3f6520ee0469504521e71` ON `users`");
        await queryRunner.query("DROP TABLE `users`");
        await queryRunner.query("DROP INDEX `IDX_8d492be5f49e0233f3567b5986` ON `genres`");
        await queryRunner.query("DROP TABLE `genres`");
    }

}
