import {MigrationInterface, QueryRunner} from "typeorm";

export class uniqueEmailsAndMemberSinceForUsers1546787366772 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `details` DROP COLUMN `memberSince`");
        await queryRunner.query("ALTER TABLE `users` ADD `memberSince` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_a8687924ae4d52f05db87f3352f`");
        await queryRunner.query("ALTER TABLE `users` ADD UNIQUE INDEX `IDX_97672ac88f789774dd47f7c8be` (`email`)");
        await queryRunner.query("ALTER TABLE `users` CHANGE `detailsId` `detailsId` int NULL");
        await queryRunner.query("ALTER TABLE `details` CHANGE `firstName` `firstName` varchar(45) NULL");
        await queryRunner.query("ALTER TABLE `details` CHANGE `lastName` `lastName` varchar(45) NULL");
        await queryRunner.query("ALTER TABLE `details` CHANGE `age` `age` int NULL");
        await queryRunner.query("ALTER TABLE `authors` CHANGE `died` `died` varchar(255) NULL");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_a8687924ae4d52f05db87f3352f` FOREIGN KEY (`detailsId`) REFERENCES `details`(`id`)");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_a8687924ae4d52f05db87f3352f`");
        await queryRunner.query("ALTER TABLE `authors` CHANGE `died` `died` varchar(255) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `details` CHANGE `age` `age` int NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `details` CHANGE `lastName` `lastName` varchar(45) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `details` CHANGE `firstName` `firstName` varchar(45) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `users` CHANGE `detailsId` `detailsId` int NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `users` DROP INDEX `IDX_97672ac88f789774dd47f7c8be`");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_a8687924ae4d52f05db87f3352f` FOREIGN KEY (`detailsId`) REFERENCES `details`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
        await queryRunner.query("ALTER TABLE `users` DROP COLUMN `memberSince`");
        await queryRunner.query("ALTER TABLE `details` ADD `memberSince` datetime(6) NULL DEFAULT 'current_timestamp(6)'");
    }

}
