import { BooksModule } from './books.module';
import { UserDetailsService } from './../services/user-details.service';
import { UserDetails } from './../database/entities/user-details.entity';
import { Module } from '@nestjs/common';
import { CoreModule } from './core/core.module';
import { UserController } from '../controllers/user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from '../services/user.service';
import { User } from '../database/entities/user.entity';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    AuthModule,
    CoreModule,
    BooksModule,
    TypeOrmModule.forFeature([User, UserDetails])],
  providers: [UserService, UserDetailsService],
  exports: [UserService, UserDetailsService],
  controllers: [UserController],
})
export class UserModule { }
