import { AuthModule } from './auth/auth.module';
import { BooksModule } from './books.module';
import { AuthorModule } from './authors.module';
import { ScrapeService } from '../services/scrape.service';
import { ScrapeController } from '../controllers/scrape.controller';
import { Module } from '@nestjs/common';
import { GenresModule } from './genres.module';

@Module({
  imports: [BooksModule, AuthorModule, AuthModule],
  providers: [ScrapeService],
  controllers: [ScrapeController],
})
export class ScrapeModule {}
