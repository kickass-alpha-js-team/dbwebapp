import { AuthModule } from './auth/auth.module';
import { GenresService } from './../services/genres.services';
import { GenresController } from './../controllers/genres.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { Genre } from '../database/entities/genre.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Genre]), AuthModule],
    controllers: [GenresController],
    providers: [GenresService],
    exports: [GenresService],
})

export class GenresModule { }
