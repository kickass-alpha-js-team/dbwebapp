import { HttpModule, Module } from '@nestjs/common';
import { BooksController } from '../controllers/books.controller';
import { BooksService } from '../services/books.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Book } from '../database/entities/book.entity';
import { AuthModule } from './auth/auth.module';
import { AuthorModule } from './authors.module';
import { GenresModule } from './genres.module';

@Module({
  imports: [AuthModule, HttpModule, AuthorModule, GenresModule, TypeOrmModule.forFeature([Book])],
  controllers: [BooksController],
  providers: [BooksService],
  exports: [BooksService],
})
export class BooksModule { }
