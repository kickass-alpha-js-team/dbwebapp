import { Module } from '@nestjs/common';
import { FileService } from '../../services/file.service';

@Module({
  imports: [],
  providers: [FileService],
  exports: [FileService],
})
export class CoreModule { }
