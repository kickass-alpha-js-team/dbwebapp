import { AuthService } from '../../services/auth/auth.service';
import { JwtStrategy } from '../../services/auth/strategy/jwt.strategy';
import { ConfigService } from '../../config/config.service';
import { ConfigModule } from '../../config/config.module';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { Module } from '@nestjs/common';
import { UserService } from '../../services/user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../../database/entities/user.entity';

@Module({
  imports: [
    ConfigModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secretOrPrivateKey: configService.jwtSecret,
        signOptions: {
          expiresIn: configService.jwtExpireTime,
        },
      }),
      inject: [ConfigService],
    }),
    TypeOrmModule.forFeature([User]),
    ],
  providers: [AuthService, JwtStrategy, UserService],
  exports: [AuthService],
  controllers: [],
})
export class AuthModule { }
