import { AuthModule } from './auth/auth.module';
import { Author } from '../database/entities/author.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { AuthorsService } from '../services/authors.service';
import { AuthorsController } from '../controllers/authors.controller';
import { GenresModule } from './genres.module';

@Module({
  imports: [TypeOrmModule.forFeature([Author]), AuthModule, GenresModule],
  providers: [AuthorsService],
  controllers: [AuthorsController],
  exports: [AuthorsService],
})
export class AuthorModule {}
