import { ExceptionFilter, Catch, ArgumentsHost, UnauthorizedException } from '@nestjs/common';

@Catch(UnauthorizedException)

export class UnauthorizedExceptionFilter implements ExceptionFilter {
  catch(exception: UnauthorizedException, host: ArgumentsHost) {
    const context = host.switchToHttp();
    const response = context.getResponse();
    const request = context.getRequest();

    exception.message.statusCode = 404,
    exception.message.error = 'Not Found';
    exception.message.message = `Cannot ${request.method} ${request.route.path}`;

    response
      .status(404)
      .send(exception.message);
  }
}
