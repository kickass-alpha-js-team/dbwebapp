import { BadRequestException, ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';

@Catch(BadRequestException)

export class BadRequestExceptionFilter implements ExceptionFilter {
  catch(exception: BadRequestException, host: ArgumentsHost) {
    const context = host.switchToHttp();
    const response = context.getResponse();
    const status = exception.getStatus();

    let message;
    if (exception.message.message) {
      message = exception.message.message;
    } else {
      if (exception.message[0].constraints.matches) {
        message = 'Your password must contain atleast 1 capital letter, 1 number, 1 special character and be atleast 6 characters long';
      } else if (exception.message[0].constraints.isEmail) {
        message = 'Your email is unvalid';
      } else {
        message = exception.message;
      }
    }

    response
      .status(status)
      .send(message);
  }
}
