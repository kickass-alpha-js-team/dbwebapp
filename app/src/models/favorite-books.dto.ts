import { IsArray } from 'class-validator';

export class FavoriteBooksDTO {
    @IsArray()
    bookTitle: string[];
}
