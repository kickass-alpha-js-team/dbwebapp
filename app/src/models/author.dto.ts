import { IsString, IsArray, Length, IsOptional } from 'class-validator';
import { Genre } from '../database/entities/genre.entity';

export class AuthorDTO {
    @IsString()
    @Length(2, 150)
    fullName: string;

    @IsString()
    born: string;

    @IsString()
    @IsOptional()
    died: string;

    @IsString()
    @Length(5, 16777215)
    biography: string;

    @IsString()
    @Length(2, 2083)
    pictureUrl: string;

    @IsArray()
    genres: Genre[];

}
