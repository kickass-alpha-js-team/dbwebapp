import { Optional } from '@nestjs/common';

export class BookScrapeSearchDTO {
    title: string;
    @Optional()
    id: number;
}
