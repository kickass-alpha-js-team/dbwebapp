import { IsString, IsNumber, IsArray } from 'class-validator';
import { Author } from '../database/entities/author.entity';
import { Genre } from '../database/entities/genre.entity';

export class BookDTO {
    @IsString()
    title: string;

    @IsNumber()
    pages: number;

    @IsString()
    issued: string;

    @IsString()
    coverUrl: string;

    @IsString()
    description: string;

    @IsString()
    ISBN: string;

    @IsArray()
    genres: Genre[];

    @IsArray()
    authors: Author[];

}
