import { IsString } from 'class-validator';

export class GenreDTO {
    @IsString()
    genreName: string;
}
