import { IsString, IsNumber, IsOptional, IsDateString } from 'class-validator';

export class UserDetailsDTO {
    @IsOptional()
    @IsString()
    firstName: string;

    @IsOptional()
    @IsString()
    lastName: string;

    @IsOptional()
    @IsNumber()
    age: number;

}
