import { IsString, Length, Matches, IsOptional, IsEmail, IsNumber } from 'class-validator';
import { Optional } from '@nestjs/common';
import { UserDetails } from '../../database/entities/user-details.entity';

export class UserRegisterDTO {

  @IsString()
  username: string;

  @IsEmail()
  email: string;

  @IsString()
  @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
  password: string;

  @Optional()
  avatarUrl: string;

  details: UserDetails;

}
