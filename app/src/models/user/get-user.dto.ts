import { IsString, Matches, IsBoolean } from 'class-validator';

export class GetUserDTO {

    @IsString()
    username: string;

    @IsString()
    @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
    password: string;

    @IsBoolean()
    isAdmin: boolean;
}
