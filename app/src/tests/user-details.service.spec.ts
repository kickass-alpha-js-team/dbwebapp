import { FavoriteBooksDTO } from './../models/favorite-books.dto';
import { BookDTO } from './../models/book.dto';
import { User } from './../database/entities/user.entity';
import { BooksService } from './../services/books.service';
import { UserDetailsService } from './../services/user-details.service';
import { Repository } from 'typeorm';
import { UserDetails } from '../database/entities/user-details.entity';
import { Test } from '@nestjs/testing';
import { UserDetailsDTO } from '../models/user/user-details.dto';
import { Book } from '../database/entities/book.entity';

describe('AuthorsService', () => {
    let userDetailsService: UserDetailsService;
    const booksService: BooksService = new BooksService(null, null, null);
    const userRepository: Repository<User> = new Repository();
    const userDetailsRepository: Repository<UserDetails> = new Repository();

    beforeEach(async () => {

        const module = await Test.createTestingModule({
            providers: [UserDetailsService,
                {
                    provide: 'UserRepository',
                    useValue: userRepository,
                },
                {
                    provide: 'UserDetailsRepository',
                    useValue: userDetailsRepository,
                },
                {
                    provide: 'BooksService',
                    useValue: booksService,
                }],
        }).compile();

        userDetailsService = module.get<UserDetailsService>(UserDetailsService);
    });

    beforeEach(() => {
        jest.clearAllMocks();
    });

    describe('updateDetails method', () => {
        it('should call userDetailsRepository and userRepository save methods to update one, few or all columns in user details table.', async () => {
            // Arrange
            const request = require('request');
            const user: User = new User();
            request.user = user;
            user.details = new UserDetails();
            const details = new UserDetailsDTO();
            details.age = 25;

            jest.spyOn(userDetailsRepository, 'save').mockImplementation(() => true);
            jest.spyOn(userRepository, 'save').mockImplementation(() => true);

            // Act
            await userDetailsService.updateDetails(request, details);

            // Assert
            expect(userDetailsRepository.save).toHaveBeenCalledTimes(1);
            expect(userRepository.save).toHaveBeenCalledTimes(1);
        });
    });

    describe('addFavoriteBooks method', () => {
        it('should call bookService getBookByTitleMethod and throw if no book with such title exists in database.', async () => {
            // Arrange
            const request = require('request');
            const user: User = new User();
            request.user = user;
            user.details = new UserDetails();
            const favoriteBook: Book = new Book();
            user.details.books = [favoriteBook];
            const book: FavoriteBooksDTO = new FavoriteBooksDTO();
            book.bookTitle = ['Harry Potter'];

            const expectedMessage: string = `You cannot add book with title "${book.bookTitle}". It does not exist in database.`;

            jest.spyOn(booksService, 'getBookByTitle').mockImplementation(() => false);

            // Act & Assert
            try {
                await userDetailsService.addFavoriteBooks(request, book);
            } catch (error) {
                expect(error.message.message).toBe(expectedMessage);
            }
        });

        it(`should throw if a book with the same title is already part of the user's favorites`, async () => {
            // Arrange
            const request = require('request');
            const user: User = new User();
            request.user = user;
            user.details = new UserDetails();
            const favoriteBook: Book = new Book();
            favoriteBook.title = 'Harry Potter';
            user.details.books = [favoriteBook];
            const book: FavoriteBooksDTO = new FavoriteBooksDTO();
            book.bookTitle = ['Harry Potter'];
            const bookFromDatabase: Book = new Book();
            bookFromDatabase.title = 'Harry Potter';

            const expectedMessage: string = `You cannot add book with title "${favoriteBook.title}". It is already part of your favorites.`;

            jest.spyOn(booksService, 'getBookByTitle').mockImplementation(() => bookFromDatabase);

            // Act & Assert
            try {
                await userDetailsService.addFavoriteBooks(request, book);
            } catch (error) {
                expect(error.message.message).toBe(expectedMessage);
            }
        });
    });

    describe('getUserDetails method', () => {
        it('should return a user entity together with their details.', async () => {
            // Arrange
            const request = require('request');
            const user: User = new User();
            request.user = user;
            user.details = new UserDetails();

            // Act
            const result: User = await userDetailsService.getUserDetails(request);

            // Assert
            expect(result).toEqual(user);
        });
    });

    describe('removeFromUserFavoriteBooks method', () => {
        it('should call bookService method getBookByTitle and throw if no book with such title is found.', async () => {
            // Arrange
            const request = require('request');
            const user: User = new User();
            request.user = user;
            user.details = new UserDetails();
            const favoriteBook: Book = new Book();
            user.details.books = [favoriteBook];
            const book: FavoriteBooksDTO = new FavoriteBooksDTO();
            book.bookTitle = ['Harry Potter'];

            const expectedMessage: string = `You cannot remove book with title "${book.bookTitle}". There is no such book in database.`;

            jest.spyOn(booksService, 'getBookByTitle').mockImplementation(() => false);

            // Act & Assert
            try {
                await userDetailsService.removeFromUserFavoriteBooks(request, book);
            } catch (error) {
                expect(error.message.message).toBe(expectedMessage);
            }
        });
    });
});
