import { GenresService } from '../services/genres.services';
import { Test } from '@nestjs/testing';
import { GenresController } from '../controllers/genres.controller';
import { GenreDTO } from '../models/genre.dto';

describe('GenresController', () => {
    const genresService = new GenresService(null);
    let genresController: GenresController;

    beforeAll(async () => {
        const module = await Test.createTestingModule({
            imports: [],
            controllers: [GenresController],
            providers: [GenresService,
                {
                    provide: GenresService,
                    useValue: genresService,
                }],
        }).compile();

        genresController = module.get<GenresController>(GenresController);
    });

    describe('addGenre should', () => {

        it('call genreService addGenre method', async () => {

            // Arrange
            const testGenre = new GenreDTO();
            jest.spyOn(genresService, 'addGenre').mockImplementation(() => {
                return 'new genre added';
            });

            // Act
            await genresController.addGenre(testGenre);

            // Assert
            expect(genresService.addGenre).toHaveBeenCalledTimes(1);
        });

    });
    describe('updateGenreByName should', () => {

        it('call genresService updateGenreByName method', async () => {

            // Arrange
            const testGenre = new GenreDTO();
            jest.spyOn(genresService, 'updateGenreByName').mockImplementation(() => {
                return 'genre updated';
            });

            // Act
            await genresController.updateGenreByName('testGenre Name', testGenre);

            // Assert
            expect(genresService.updateGenreByName).toHaveBeenCalledTimes(1);
        });
    });

    describe('getAll should', () => {

        it('call genresService getAll method', async () => {

            // Arrange
            jest.spyOn(genresService, 'getAll').mockImplementation(() => {
                return 'all genres';
            });

            // Act
            await genresController.getAll();

            // Assert
            expect(genresService.getAll).toHaveBeenCalledTimes(1);
        });

    });
});
