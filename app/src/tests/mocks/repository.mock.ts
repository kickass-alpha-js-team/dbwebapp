import { Repository, Entity, ObjectLiteral } from 'typeorm';
export class MockRepository extends Repository<ObjectLiteral> {

    findOne() {
        return null;
    }

    create() {

        return null;
    }
    save() {
        return null;
    }

}
