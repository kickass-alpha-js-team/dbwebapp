import { Genre } from './../database/entities/genre.entity';
import { AuthorDTO } from './../models/author.dto';
import { AuthorsService } from './../services/authors.service';
import { Author } from './../database/entities/author.entity';
import { GenresService } from './../services/genres.services';
import { Repository } from 'typeorm';
import { Test } from '@nestjs/testing';

describe('AuthorsService', () => {
    let authorsService: AuthorsService;
    const genresService: GenresService = new GenresService(null);
    const authorsRepository: Repository<Author> = new Repository();

    beforeEach(async () => {

        const module = await Test.createTestingModule({
            providers: [AuthorsService,
                {
                    provide: 'AuthorRepository',
                    useValue: authorsRepository,
                },
                {
                    provide: 'GenresService',
                    useValue: genresService,
                }],
        }).compile();

        authorsService = module.get<AuthorsService>(AuthorsService);
    });

    beforeEach(() => {
        jest.clearAllMocks();
    });

    describe('getAuthor method', () => {
        it('should call authorsRepository method findOne which returns an author entity if a corresponding entity is found.', async () => {
            // Arrange
            const author: Author = new Author();
            jest.spyOn(authorsRepository, 'findOne').mockImplementation(() => author);

            // Act
            const result = await authorsService.getAuthor(author);

            // Assert
            expect(authorsRepository.findOne).toHaveBeenCalledTimes(1);
            expect(result).toBe(author);
        });
    });

    describe('getAuthor method', () => {
        it('should call authorsService method addAuthor and add the searched author if no corresponding entity is found.', async () => {
            // Arrange
            const author: Author = new Author();

            jest.spyOn(authorsRepository, 'findOne').mockImplementation(() => false);
            jest.spyOn(authorsService, 'addAuthor').mockImplementation(() => 'added');

            // Act
            await authorsService.getAuthor(author);

            // Assert
            expect(authorsService.addAuthor).toBeCalledTimes(1);
        });
    });

    describe('addAuthor method', () => {
        it(`should return a success message when an author is added to database`, async () => {
            // Arrange
            const authorDTO: AuthorDTO = new AuthorDTO();
            authorDTO.fullName = 'J. R. R. Tolkein';
            const genre: Genre = new Genre();
            authorDTO.genres = [genre];

            jest.spyOn(authorsRepository, 'findOne').mockImplementation(() => false);
            jest.spyOn(genresService, 'getGenre').mockImplementation(() => 'found');
            jest.spyOn(authorsRepository, 'create').mockImplementation(() => 'created');
            jest.spyOn(authorsRepository, 'save').mockImplementation(() => 'saved');
            const expectedResult: string = `Author with name "${authorDTO.fullName}" was successfully added to database.`;

            // Act
            const result = await authorsService.addAuthor(authorDTO);

            // Assert
            expect(authorsRepository.save).toHaveBeenCalledTimes(1);
            expect(result).toBe(expectedResult);
        });
    });

    describe('addAuthor method', () => {
        it('should throw a custom error message if an author with the same name is found in the database.', async () => {
            // Arrange
            const authorDTO: AuthorDTO = new AuthorDTO();
            authorDTO.fullName = 'J. R. R. Tolkein';

            jest.spyOn(authorsRepository, 'findOne').mockImplementation(() => 'found');

            const result: string = `Author with name '${authorDTO.fullName}' already exists in database.`;

            // Act & Assert
            try {
                await authorsService.addAuthor(authorDTO);
            } catch (error) {
                expect(error.message.message).toBe(result);
            }
        });
    });

    describe('updateAuthorByName method', () => {
        it('should call authorsRepository findOne method to find an author by full name, update him/her and return a success message.', async () => {
            // Arrange
            const authorFromDatabase: Author = new Author();
            authorFromDatabase.fullName = 'Rowling';

            const updatedVersion: AuthorDTO = new AuthorDTO();
            updatedVersion.fullName = 'J. K. Rowling';

            jest.spyOn(authorsRepository, 'findOne').mockImplementation(() => 'found');
            jest.spyOn(authorsRepository, 'update').mockImplementation(() => 'updated');

            const expectedResult: string = `Author with name "${updatedVersion.fullName}" was successfully updated.`;

            // Act
            const result: string = await authorsService.updateAuthorByName(authorFromDatabase.fullName, updatedVersion);

            // Assert
            expect(authorsRepository.findOne).toHaveBeenCalledTimes(1);
            expect(authorsRepository.update).toHaveBeenCalledTimes(1);
            expect(result).toBe(expectedResult);
        });
    });

    describe('updateAuthorByName method', () => {
        it('should throw if an author with a corresponding full name is not found in database', async () => {
            // Arrange
            const authorFromDatabase: Author = new Author();
            authorFromDatabase.fullName = 'Rowling';

            const updatedVersion: AuthorDTO = new AuthorDTO();

            jest.spyOn(authorsRepository, 'findOne').mockImplementation(() => false);

            const result: string = `Author with name "${authorFromDatabase.fullName}" does not exist in database.`;

            // Act $ Assert
            try {
                await authorsService.updateAuthorByName(authorFromDatabase.fullName, updatedVersion);
            } catch (error) {
                expect(error.message.message).toBe(result);
            }
        });
    });

    describe('getAll method', () => {
        it('should call the authorsRepository find method and return all authors present in database.', async () => {
            // Arrange
            const author: Author = new Author();
            const author2: Author = new Author();
            const authors: Author[] = [author, author2];
            jest.spyOn(authorsRepository, 'find').mockImplementation(() => authors);

            // Act
            const result: Author[] = await authorsService.getAll();

            // Assert
            expect(result).toEqual(authors);
        });
    });

    describe('getAuthorByName method', () => {
        it('should call authorsRepository findOne and return an author with a corresponding full name.', async () => {
            // Arrange
            const name: string = 'Rowling';
            const author: Author = new Author();
            author.fullName = name;

            jest.spyOn(authorsRepository, 'findOne').mockImplementation(() => author);

            // Act
            const result: Author = await authorsService.getAuthorByName(name);

            // Assert
            expect(result).toBe(author);
        });
    });

    describe('getAuthorByName method', () => {
        it('should throw error when no author with a corresponding full name is found.', async () => {
            // Arrange
            const name: string = 'Rowling';

            jest.spyOn(authorsRepository, 'findOne').mockImplementation(() => false);

            const result: string = `Author with name "${name}" does not exist in database.`;

            // Act & Assert
            try {
                await authorsService.getAuthorByName(name);
            } catch (error) {
                expect(error.message.message).toBe(result);
            }
        });
    });

    describe('getAuthorsByFirstLetters method', () => {
        it('should call authorsRepository find method and return an array of author/s whose initial letter/s match the searched ones.', async () => {
            // Arrange
            const letters: string = 'Row';
            const authorFromDatabase: Author = new Author();
            authorFromDatabase.fullName = 'Rowling';

            jest.spyOn(authorsRepository, 'find').mockImplementation(() => authorFromDatabase);

            // Act
            const result: Author[] = await authorsService.getAuthorsByFirstLetters(letters);

            // Assert
            expect(result).toEqual(authorFromDatabase);
        });
    });

    describe('getAuthorsByFirstLetters method', () => {
        it('should throw error when no author whose name begins with the searched letters is found.', async () => {
            // Arrange
            const letters: string = 'Pe';
            const authorFromDatabase: Author = new Author();
            authorFromDatabase.fullName = 'Rowling';

            jest.spyOn(authorsRepository, 'find').mockImplementation(() => false);

            const result: string = `Sorry, no author/s with initial letter/s "${letters}" were found.`;
            // Act & Assert
            try {
                authorsService.getAuthorsByFirstLetters(letters);
            } catch (error) {
                expect(error.message.message).toBe(result);
            }
        });
    });
});
