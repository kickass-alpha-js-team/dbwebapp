import { Test } from '@nestjs/testing';
import { BooksService } from './../services/books.service';
import { AuthorsService } from '../services/authors.service';
import { GenresService } from '../services/genres.services';
import { Book } from '../database/entities/book.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { AuthorDTO } from '../models/author.dto';
import { Author } from '../database/entities/author.entity';
import { Genre } from '../database/entities/genre.entity';

describe('BooksService', () => {
    let booksService: BooksService;
    const authorsService: AuthorsService = new AuthorsService(null, null);
    const genresService: GenresService = new GenresService(null);
    const booksMockRepository = {
        find: () => {
            return 'all genres array';
        },
        findOne: () => {
            return 'genre';
        },
        create: () => {
            return 'genre created';
        },
        save: () => {
            return 'genre saved';
        },
        update: () => {
            return 'updated';
        },
    };

    beforeAll(async () => {
        const module = await Test.createTestingModule({
            providers: [BooksService,
                {
                    provide: getRepositoryToken(Book),
                    useValue: booksMockRepository,
                },
                {
                    provide: 'AuthorsService',
                    useValue: authorsService,
                },
                {
                    provide: 'GenresService',
                    useValue: genresService,
                },
            ],
        }).compile();

        booksService = module.get<BooksService>(BooksService);
    });
    describe('addBook method', () => {
        it('should call booksRepository findOne method and throw error if the passed book name is already in the database', async () => {
            // Arrange
            const testBook: Book = new Book();
            jest.spyOn(booksMockRepository, 'findOne').mockImplementation(() => true);

            try {
                // Act
                const result = await booksService.addBook(testBook);

                // Assert
                expect(booksMockRepository.findOne).toHaveBeenCalled();
                expect(result).toThrowError();

                // tslint:disable-next-line:no-empty
            } catch  { }
        });

        it('should call getAuthor method of authorsService for all authors of the passed book', async () => {

            // Arrange
            const testBook: Book = new Book();
            const testAuthor: Author = new Author();
            const testGenre: Genre = new Genre();
            testBook.authors = [testAuthor];
            testBook.genres = [testGenre];
            jest.spyOn(booksMockRepository, 'findOne').mockImplementation(() => false);
            jest.spyOn(authorsService, 'getAuthor').mockImplementation(() => testAuthor);
            jest.spyOn(genresService, 'getGenre').mockImplementation(() => testGenre);

            // Act
            await booksService.addBook(testBook);

            // Assert
            expect(booksMockRepository.findOne).toHaveBeenCalled();
            expect(authorsService.getAuthor).toHaveBeenCalled();

        });

        it('should call getGenre method of genresService for all genres of the passed book', async () => {

            // Arrange
            const testBook: Book = new Book();
            const testAuthor: Author = new Author();
            const testGenre: Genre = new Genre();
            testBook.authors = [testAuthor];
            testBook.genres = [testGenre];
            jest.spyOn(booksMockRepository, 'findOne').mockImplementation(() => false);
            jest.spyOn(authorsService, 'getAuthor').mockImplementation(() => testAuthor);
            jest.spyOn(genresService, 'getGenre').mockImplementation(() => testGenre);

            // Act
            await booksService.addBook(testBook);

            // Assert
            expect(booksMockRepository.findOne).toHaveBeenCalled();
            expect(genresService.getGenre).toHaveBeenCalled();

        });

        it('should call create and save methods of booksRepository', async () => {

            // Arrange
            const testBook: Book = new Book();
            const testAuthor: Author = new Author();
            const testGenre: Genre = new Genre();
            testBook.authors = [testAuthor];
            testBook.genres = [testGenre];
            jest.spyOn(booksMockRepository, 'findOne').mockImplementation(() => false);
            jest.spyOn(authorsService, 'getAuthor').mockImplementation(() => testAuthor);
            jest.spyOn(genresService, 'getGenre').mockImplementation(() => testGenre);
            jest.spyOn(booksMockRepository, 'save');
            jest.spyOn(booksMockRepository, 'create');

            // Act
            await booksService.addBook(testBook);

            // Assert
            expect(booksMockRepository.create).toHaveBeenCalled();
            expect(booksMockRepository.save).toHaveBeenCalled();

        });
    });

    describe('updateBookByTitle', () => {
        it('should call booksRepository findOne method and throw error if the passed book name is not found in the database', async () => {
            // Arrange
            const testBook: Book = new Book();
            jest.spyOn(booksMockRepository, 'findOne').mockImplementation(() => false);

            try {
                // Act
                const result = await booksService.updateBookByTitle('test Title', testBook);

                // Assert
                expect(booksMockRepository.findOne).toHaveBeenCalled();
                expect(result).toThrowError();

                // tslint:disable-next-line:no-empty
            } catch  { }
        });
        it('should call booksRepository if the passed book is found in the database', async () => {
            // Arrange
            const testBook: Book = new Book();
            jest.spyOn(booksMockRepository, 'findOne').mockImplementation(() => testBook);
            jest.spyOn(booksMockRepository, 'update');
            // Act
            const result = await booksService.updateBookByTitle('test Title', testBook);

            // Assert
            expect(booksMockRepository.update).toHaveBeenCalled();
        });
    });

    describe('getAll', () => {
        it('should call booksRepository find method', async () => {
            // Arrange
            jest.spyOn(booksMockRepository, 'find');

            // Act
            await booksService.getAll();

            // Assert
            expect(booksMockRepository.find).toHaveBeenCalled();
        });
    });

    describe('getBookByTitle', () => {
        it('should call booksRepository findOne method and throw error if the passed book title is not found in the database', async () => {
            // Arrange
            jest.spyOn(booksMockRepository, 'findOne').mockImplementation(() => false);

            try {
                // Act
                const result = await booksService.getBookByTitle('test Title');

                // Assert
                expect(booksMockRepository.findOne).toHaveBeenCalled();
                expect(result).toThrowError();

                // tslint:disable-next-line:no-empty
            } catch  { }
        });

        it('should call booksRepository findOne method if there is book with the passed title in the database', async () => {
            // Arrange
            const testBook = new Book();
            jest.spyOn(booksMockRepository, 'findOne').mockImplementation(() => testBook);

            // Act
            await booksService.getBookByTitle(testBook.title);

            // Assert
            expect(booksMockRepository.findOne).toHaveBeenCalled();
        });
    });

    describe('getBooksByFirstLetters', () => {
        it('should call booksRepository findOne method and throw error if there is no book title starting with the passed letter in the database',
         async () => {
            // Arrange
            jest.spyOn(booksMockRepository, 'find').mockImplementation(() => false);

            try {
                // Act
                const result = await booksService.getBooksByFirstLetters('A');

                // Assert
                expect(booksMockRepository.find).toHaveBeenCalled();
                expect(result).toThrowError();

                // tslint:disable-next-line:no-empty
            } catch  { }
        });

        it('should call booksRepository findOne method if there are books which titles start with the passed letter in the database', async () => {
            // Arrange
            const testBook = new Book();
            jest.spyOn(booksMockRepository, 'find').mockImplementation(() => [testBook]);

            // Act
            await booksService.getBooksByFirstLetters('A');

            // Assert
            expect(booksMockRepository.find).toHaveBeenCalled();
        });
    });
});
