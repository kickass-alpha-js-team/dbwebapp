
import { Test } from '@nestjs/testing';
import { BooksService } from '../services/books.service';
import { BooksController } from '../controllers/books.controller';
import { AuthorsService } from '../services/authors.service';
import { GenresService } from '../services/genres.services';
import { BookDTO } from '../models/book.dto';
import { PassportModule } from '@nestjs/passport';

describe('BooksController', () => {
  const booksService = new BooksService(null, null, null);
  const authorsService = new AuthorsService(null, null);
  const genresService = new GenresService(null);
  let booksController: BooksController;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [/* PassportModule.register({
        defaultStrategy: 'jwt',
      }) */],
      controllers: [BooksController],
      providers: [AuthorsService, GenresService,
        {
          provide: BooksService,
          useValue: booksService,
        },
        {
          provide: AuthorsService,
          useValue: authorsService,
        },
        {
          provide: GenresService,
          useValue: genresService,
        }],
    }).compile();

    booksController = module.get<BooksController>(BooksController);
  });
  describe('add should', () => {
    it('call booksService addBook method', async () => {

      // Arrange
      const testBook = new BookDTO();
      jest.spyOn(booksService, 'addBook').mockImplementation(() => {
        return 'test book added';
      });

      // Act
      await booksController.add(testBook);

      // Assert
      expect(booksService.addBook).toHaveBeenCalledTimes(1);
    });
  });

  describe('updateBookByName should', () => {
    it('call booksService updateBookByTitle method', async () => {

      // Arrange
      const testBook = new BookDTO();
      jest.spyOn(booksService, 'updateBookByTitle').mockImplementation(() => {
        return 'test book updated';
      });

      // Act
      await booksController.updateBookByName('title', testBook);

      // Assert
      expect(booksService.updateBookByTitle).toHaveBeenCalledTimes(1);
    });
  });

  describe('getAll should', () => {
    it('call booksService getAll method', async () => {

      // Arrange
      jest.spyOn(booksService, 'getAll').mockImplementation(() => {
        return 'all books returned';
      });

      // Act
      await booksController.getAll();

      // Assert
      expect(booksService.getAll).toHaveBeenCalledTimes(1);
    });
  });

  describe('findByTitle should', () => {
    it('call booksService getBookByTitle method', async () => {

      // Arrange
      jest.spyOn(booksService, 'getBookByTitle').mockImplementation(() => {
        return 'books with that title';
      });

      // Act
      await booksController.findByTitle('test title');

      // Assert
      expect(booksService.getBookByTitle).toHaveBeenCalledTimes(1);
    });
  });

  describe('getBooksByFirstLetters should', () => {
    it('call booksService getBooksByFirstLetters method', async () => {

      // Arrange
      jest.spyOn(booksService, 'getBooksByFirstLetters').mockImplementation(() => {
        return 'books with titles starting with that letter';
      });

      // Act
      await booksController.getBooksByFirstLetters('l');

      // Assert
      expect(booksService.getBooksByFirstLetters).toHaveBeenCalledTimes(1);
    });
  });

});
