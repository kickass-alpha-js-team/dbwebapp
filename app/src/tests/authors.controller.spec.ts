import { Author } from './../database/entities/author.entity';
import { AuthorDTO } from './../models/author.dto';
import { Test } from '@nestjs/testing';
import { AuthorsController } from './../controllers/authors.controller';
import { AuthorsService } from './../services/authors.service';

describe('AuthorsController', () => {
    const authorsService: AuthorsService = new AuthorsService(null, null);
    let authorsController: AuthorsController;

    beforeAll(async () => {

        const module = await Test.createTestingModule({
            controllers: [AuthorsController],
            providers: [AuthorsService, {
                provide: AuthorsService,
                useValue: authorsService,
            }],
        }).compile();

        authorsController = module.get<AuthorsController>(AuthorsController);
    });

    describe('addAuthor', async () => {
        it(`should call authorsService addAuthor method and return a string containing the added author's name`, async () => {
            // Arrange
            const author: AuthorDTO = new AuthorDTO();
            author.fullName = 'J. R. R. Tolkein';
            const expectedResult: string = `Author with name "${author.fullName}" was successfully added to database.`;
            jest.spyOn(authorsService, 'addAuthor').mockImplementation( () => expectedResult);

            // Act
            const result = await authorsController.addAuthor(author);

            // Assert
            expect(authorsService.addAuthor).toHaveBeenCalledTimes(1);
            expect(result).toBe(expectedResult);
        });
    });

    describe('updateAuthorByName', async () => {
        it(`should call authorsService updateAuthorByName method and return a string containing the updated author's name.`, async () => {
            // Arrange
            const name: string = 'J. R. R. Tolk';
            const author: AuthorDTO = new AuthorDTO();
            author.fullName = 'J. R. R. Tolkein';
            const expectedResult: string = `Author with name "${author.fullName}" was successfully updated.`;

            jest.spyOn(authorsService, 'updateAuthorByName').mockImplementation(() => expectedResult);

            // Act
            const result = await authorsController.updateAuthorByName(name, author);

            // Assert
            expect(authorsService.updateAuthorByName).toBeCalledTimes(1);
            expect(result).toBe(expectedResult);
        });
    });

    describe('getAll', async () => {
        it('should call authorService method getAll and return an array of author entities', async () => {
            // Arrange
            const author1: Author = new Author();
            const author2: Author = new Author();
            const expectedResult = [author1, author2];
            jest.spyOn(authorsService, 'getAll').mockImplementation(() => expectedResult);

            // Act
            const result = await authorsController.getAll();

            // Assert
            expect(authorsService.getAll).toBeCalledTimes(1);
            expect(result).toBe(expectedResult);
        });
    });

    describe('findByName', async () => {
        it(`should call authorsService method getAuthorByName with the author's full name once.`, async () => {
            // Arrange
            const fullName: string = 'J. R. R. Tolkein';
            jest.spyOn(authorsService, 'getAuthorByName').mockImplementation(() => 'found');

            // Act
            await authorsController.findByName(fullName);

            // Assert
            expect(authorsService.getAuthorByName).toBeCalledTimes(1);
            expect(authorsService.getAuthorByName).toBeCalledWith(fullName);
        });
    });

    describe('findByName', async () => {
        it(`should call authorsService method getAuthorByName and return an author entity.`, async () => {
            // Arrange
            const fullName: string = 'J. R. R. Tolkein';
            const author: Author = new Author();
            author.fullName = fullName;
            jest.spyOn(authorsService, 'getAuthorByName').mockImplementation(() => author);

            // Act & Assert
            expect(await authorsController.findByName(fullName)).toBe(author);
        });
    });

    describe('getAuthorsByFirstLetters', async () => {
        it(`should call authorService method getAuthorsByFirstLetters with the author's first letters once.`, async () => {
            // Arrange
            const firstLetters: string = 'J. R.';
            jest.spyOn(authorsService, 'getAuthorsByFirstLetters').mockImplementation(() => 'found');

            // Act
            await authorsController.getAuthorsByFirstLetters(firstLetters);

            // Assert
            expect(authorsService.getAuthorsByFirstLetters).toBeCalledTimes(1);
            expect(authorsService.getAuthorsByFirstLetters).toBeCalledWith(firstLetters);
        });
    });

    describe('getAuthorsByFirstLetters', async () => {
        it(`should return an author entity whose name begins with the letters given.`, async () => {
            // Arrange
            const firstLetters: string = 'J. R.';
            const author: Author = new Author();
            author.fullName = 'J. R. R. Tolkein';
            jest.spyOn(authorsService, 'getAuthorsByFirstLetters').mockImplementation(() => author);

            // Assert
            expect(await authorsController.getAuthorsByFirstLetters(firstLetters)).toBe(author);
        });
    });
});
