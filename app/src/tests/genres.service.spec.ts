import { GenresService } from '../services/genres.services';
import { Genre } from '../database/entities/genre.entity';
import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('GenresService', () => {

    let genresService: GenresService;

    const genresMockRepository = {
        find: () => {
            return 'all genres array';
        },
        findOne: () => {
            return 'genre';
        },
        create: () => {
            return 'genre created';
        },
        save: () => {
            return 'genre saved';
        },
        update: () => {
            return 'updated';
        },
    };

    beforeAll(async () => {
        const module = await Test.createTestingModule({
            providers: [GenresService,
                {
                    provide: getRepositoryToken(Genre),
                    useValue: genresMockRepository,
                }],
        }).compile();

        genresService = module.get<GenresService>(GenresService);
    });

    describe('getGenre method', () => {
        it('should call genresRepository findOne method and return genre if found', async () => {
            // Arrange
            const testGenre: Genre = new Genre();
            jest.spyOn(genresMockRepository, 'findOne').mockImplementation(() => testGenre);

            // Act
            const result = await genresService.getGenre(testGenre);

            // Assert
            expect(genresMockRepository.findOne).toHaveBeenCalledTimes(1);
            expect(result).toBe(testGenre);
        });

        it('should call genresRepository findOne method and genresService addGenre method if genres is not found', async () => {
            // Arrange
            const testGenre: Genre = new Genre();
            jest.spyOn(genresMockRepository, 'findOne').mockImplementation(() => false);
            jest.spyOn(genresService, 'addGenre');
            // Act
            const result = await genresService.getGenre(testGenre);

            // Assert
            expect(genresService.addGenre).toHaveBeenCalledWith(testGenre);
        });

    });

    describe('addGenre method', () => {
        it('should call genresRepository findOne method and throw error if the passed genre name is already in the database', async () => {
            // Arrange

            const testGenre: Genre = new Genre();
            jest.spyOn(genresMockRepository, 'findOne').mockImplementation(() => true);

            try {
                // Act
                const result = await genresService.addGenre(testGenre);

                // Assert
                expect(genresMockRepository).toHaveBeenCalled();
                expect(result).toThrowError();

                // tslint:disable-next-line:no-empty
            } catch  { }
        });

        it('should call genresRepository create method if the passed genre is not found in the database', async () => {
            // Arrange
            const testGenre: Genre = new Genre();
            jest.spyOn(genresMockRepository, 'findOne').mockImplementation(() => false);
            jest.spyOn(genresMockRepository, 'create');
            jest.spyOn(genresMockRepository, 'save');

            // Act
            await genresService.addGenre(testGenre);

            // Assert
            expect(genresMockRepository.create).toHaveBeenCalled();
            expect(genresMockRepository.save).toHaveBeenCalled();

        });

    });

    describe('updateGenreByName method', () => {
        it('should call genresRepository findOne method and throw if passed genre is not found in the database', async () => {
            // Arrange

            const testGenre: Genre = new Genre();
            jest.spyOn(genresMockRepository, 'findOne').mockImplementation(() => false);

            try {
                // Act
                const result = await genresService.updateGenreByName('name' , testGenre);

                // Assert
                expect(genresMockRepository).toHaveBeenCalled();
                expect(result).toThrowError();

                // tslint:disable-next-line:no-empty
            } catch  { }
        });
        it('should call genresRepository update method if passed genre is found in the database', async () => {
            // Arrange
            const testGenre: Genre = new Genre();
            jest.spyOn(genresMockRepository, 'findOne').mockImplementation(() => testGenre);
            jest.spyOn(genresMockRepository, 'update');

            // Act
            await genresService.updateGenreByName('name' , testGenre);

            // Assert
            expect(genresMockRepository.update).toHaveBeenCalled();
        });
    });

    describe('getAll method', () => {
        it('should call genresRepository find method', async () => {
            // Arrange
            jest.spyOn(genresMockRepository, 'find');

            // Act
            await genresService.getAll();

            // Assert
            expect(genresMockRepository.find).toHaveBeenCalled();
        });
    });
});
