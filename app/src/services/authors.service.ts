import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Author } from '../database/entities/author.entity';
import { Repository, Like } from 'typeorm';
import { AuthorDTO } from '../models/author.dto';
import { GenresService } from './genres.services';

@Injectable()
export class AuthorsService {
    constructor(
        @InjectRepository(Author) private readonly authorsRepository: Repository<Author>,
        private readonly genresService: GenresService) { }

    async getAuthor(author: Author) {
        const authorFound = await this.authorsRepository.findOne({ where: { fullName: author.fullName } });
        if (authorFound) {
            author = authorFound;
        } else {
            await this.addAuthor(author);
            const savedAuthor = await this.authorsRepository.findOne({ where: { fullName: author.fullName } });
            author = savedAuthor;
        }
        return author;
    }

    async addAuthor(author: AuthorDTO) {
        const authorFound = await this.authorsRepository.findOne({ where: { fullName: author.fullName } });

        if (authorFound) {
            throw new BadRequestException(`Author with name '${author.fullName}' already exists in database.`);
        }

        for (const genre of author.genres) {
            const takenGenre = await this.genresService.getGenre(genre);
            genre.id = takenGenre.id;
        }
        await this.authorsRepository.create(author);
        await this.authorsRepository.save([author]);

        return `Author with name "${author.fullName}" was successfully added to database.`;
    }
    async updateAuthorByName(name, authorDTO: AuthorDTO): Promise<string> {
        const authorToUpdate = await this.authorsRepository.findOne({ where: { fullName: name } });

        if (!authorToUpdate) {
            throw new BadRequestException(`Author with name "${name}" does not exist in database.`);
        }
        await this.authorsRepository.update(authorToUpdate, authorDTO);

        return `Author with name "${authorDTO.fullName}" was successfully updated.`;
    }

    async getAll(): Promise<Author[]> {
        return await this.authorsRepository.find();
    }

    async getAuthorByName(name): Promise<Author> {
        const authorFound: Author = await this.authorsRepository.findOne({ where: { fullName: name } });

        if (!authorFound) {
            throw new BadRequestException(`Author with name "${name}" does not exist in database.`);
        }

        return authorFound;
    }

    async getAuthorsByFirstLetters(letter): Promise<Author[]> {
        const authorsFound: Author[] = await this.authorsRepository.find({ fullName: Like(`${letter}%`) });

        if (!authorsFound) {
            throw new BadRequestException(`Sorry, no author/s with initial letter/s "${letter}" were found.`);
        }
        return authorsFound;
    }
}
