import { UserDetails } from './../database/entities/user-details.entity';
import { Injectable, BadRequestException, Optional } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { JwtPayload } from '../interfaces/jwt-payload';
import { User } from '../database/entities/user.entity';
import { UserRegisterDTO } from '../models/user/user-register.dto';
import { GetUserDTO } from '../models/user/get-user.dto';
import { UserLoginDTO } from '../models/user/user-login.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @Optional() @InjectRepository(UserDetails) private readonly userDetailsRepository: Repository<UserDetails>,

  ) { }

  async registerUser(user: UserRegisterDTO): Promise<string> {
    const userFound = await this.usersRepository.findOne({ where: { username: user.username } });

    if (userFound) {
      throw new BadRequestException(`Username "${userFound.username}" is already in use. Please, choose another one!`);
    }

    user.password = await bcrypt.hash(user.password, 10);

    const details: UserDetails = new UserDetails();
    await this.userDetailsRepository.save(details);
    await this.usersRepository.create(user);
    user.details = details;
    await this.usersRepository.save([user]);

    return 'Registration successful';
  }

  async validateUser(payload: JwtPayload): Promise<GetUserDTO> {
    const userFound: any = await this.usersRepository.findOne({ where: { username: payload.username } });
    return userFound;
  }

  async signIn(user: UserLoginDTO): Promise<GetUserDTO> {
    const userFound: GetUserDTO = await this.usersRepository
      .findOne({ select: ['username', 'isAdmin', 'password'], where: { username: user.username } });

    if (userFound) {
      const result = await bcrypt.compare(user.password, userFound.password);
      if (result) {
        return userFound;
      }
    }

    return null;
  }

  async getAll(): Promise<User[]> {
    return this.usersRepository.find({});
  }

  async getUserById(userID): Promise<User> {
    const userFound: User = await this.usersRepository.findOne({ where: { id: userID } });

    if (!userFound) {
      throw new BadRequestException(`Sorry! No user with id "${userID}" found.`);
    }
    return userFound;
  }
}
