import { Genre } from './../database/entities/genre.entity';
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { GenreDTO } from '../models/genre.dto';

@Injectable()
export class GenresService {
    constructor(@InjectRepository(Genre) private readonly genresRepository: Repository<Genre>) { }

    async getGenre(genre: Genre): Promise<Genre> {
        const genreFound = await this.genresRepository.findOne({ where: { genreName: genre.genreName } });
        if (genreFound) {
            genre = genreFound;
        } else {
            await this.addGenre(genre);
            const savedGenre = await this.genresRepository.findOne({ where: { genreName: genre.genreName } });
            genre = savedGenre;
        }
        return genre;
    }

    async addGenre(genre: GenreDTO): Promise<string> {
        const genreFound: Genre = await this.genresRepository.findOne({ where: { genreName: genre.genreName } });

        if (genreFound) {
            throw new BadRequestException(`Genre with name "${genre.genreName}" already exists in database.`);
        }

        await this.genresRepository.create(genre);
        await this.genresRepository.save([genre]);
        return `Genre "${genre.genreName}" was successfully added to database.`;
    }

    async updateGenreByName(name: string, newGenre: GenreDTO): Promise<string> {
        const genreToUpdate: Genre = await this.genresRepository.findOne({ where: { name: newGenre.genreName } });

        if (!genreToUpdate) {
            throw new BadRequestException(`Genre with name "${newGenre.genreName}" does not exist in database.`);
        }
        await this.genresRepository.update(genreToUpdate, newGenre);

        return `Genre with name "${newGenre.genreName}" was successfully updated.`;
    }

    async getAll(): Promise<Genre[]> {
        return await this.genresRepository.find();
    }
}
