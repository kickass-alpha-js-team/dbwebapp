import { BooksService } from './books.service';
import { User } from '../database/entities/user.entity';
import { FavoriteBooksDTO } from './../models/favorite-books.dto';
import { UserDetailsDTO } from './../models/user/user-details.dto';
import { UserDetails } from './../database/entities/user-details.entity';
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Connection, getConnection } from 'typeorm';
import { Book } from '../database/entities/book.entity';

@Injectable()
export class UserDetailsService {
    constructor(
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(UserDetails) private readonly userDetailsRepository: Repository<UserDetails>,
        private readonly booksService: BooksService,
    ) { }

    async updateDetails(req, detailsDTO: UserDetailsDTO): Promise<UserDetails> {
        const userFound: User = req.user;
        const details = userFound.details;
        details.age = detailsDTO.age;
        details.firstName = detailsDTO.firstName;
        details.lastName = detailsDTO.lastName;

        const saveDetails = await this.userDetailsRepository.save(details);

        userFound.details = saveDetails;
        await this.usersRepository.save(userFound);

        return details;
    }

    async addFavoriteBooks(req, favoriteBooks: FavoriteBooksDTO): Promise<string> {
        const userFound: User = req.user;
        const userDetails = userFound.details;
        const books: Book[] = userDetails.books;

        for (const bookTitle of favoriteBooks.bookTitle) {
            const bookFound = await this.booksService.getBookByTitle(bookTitle);

            if (!bookFound) {
                throw new BadRequestException(`You cannot add book with title "${bookTitle}". It does not exist in database.`);
            }

            const bookAlreadyExists: Book = books.find(book => {
                return book.id === bookFound.id;
            });
            if (bookAlreadyExists) {
                throw new BadRequestException(`You cannot add book with title "${bookAlreadyExists.title}". It is already part of your favorites.`);
            }

            userDetails.books.push(bookFound);
            await this.userDetailsRepository.save(userDetails);
        }
        return `Congratulations! You've updated your list of favorite books successfully! Enjoy your readings!`;
    }

    async getUserDetails(req): Promise<User> {
        const userFound: User = req.user;

        return userFound;
    }

    async removeFromUserFavoriteBooks(req, favoriteBooks: FavoriteBooksDTO): Promise<string> {
        const userFound: User = req.user;
        const userDetails = userFound.details;
        const userDetailsId: number = userDetails.id;
        const books: Book[] = userDetails.books;

        for (const bookTitle of favoriteBooks.bookTitle) {
            const bookFound = await this.booksService.getBookByTitle(bookTitle);

            if (!bookFound) {
                throw new BadRequestException(`You cannot remove book with title "${favoriteBooks.bookTitle}". There is no such book in database.`);
            }

            const bookExistsInFavorites: Book = books.find(book => {
                return book.id === bookFound.id;
            });

            if (!bookExistsInFavorites) {
                throw new BadRequestException(`You cannot remove book with title "${bookFound.title}". It is not part of your favorites yet.`);
            }

            const connection: Connection = await getConnection();
            await connection
                .createQueryBuilder()
                .relation(UserDetails, 'books')
                .of({ id: userDetailsId })
                .remove({ id: bookFound.id });

            return `You have just removed a book with title "${bookFound.title}" from your list of favorites.`;
        }
    }
}
