import { Injectable, HttpService } from '@nestjs/common';
import { Author } from '../database/entities/author.entity';
import { Genre } from '../database/entities/genre.entity';
import { BookDTO } from '../models/book.dto';

// tslint:disable-next-line:no-var-requires
const cheerio = require('cheerio');

@Injectable()
export class ScrapeService {
  constructor(
    private readonly httpService: HttpService,
  ) { }

  bookSearch(bookTitle) {
    const url: string = `https://www.goodreads.com/search?utf8=%E2%9C%93&q=${bookTitle}&search_type=books&search%5Bfield%5D=title`;
    return this.httpService.get(url).toPromise()
      .then(body => {
        const booksArr = [];
        const $ = cheerio.load(body.data);
        $('.bookTitle').each((i, el) => {
          const element: any = $(el);
          const title = element.text().trim();
          const link: string = `https://www.goodreads.com${element.attr('href').split('?')[0]}`;
          const currBook: any = {
            ID: i + 1,
            title,
            link,
          };
          booksArr.push(currBook);
        });
        return booksArr;
      });
  }

  public async addBook(bookUrl: string) {
    const body = await this.httpService.get(bookUrl).toPromise();

    const $ = cheerio.load(body.data);

    const title = $('#bookTitle').text().trim();

    const pages = +$('span[itemprop="numberOfPages"]').text().trim()
      .split(' ')[0];

    const issued = $('div.row:contains("Published")').text().trim()
      .split('Published')[1].trim()
      .split('by')[0].trim();

    const coverUrl = $('img#coverImage').attr('src');

    const isbn = $('div.infoBoxRowTitle:contains("ISBN")').next()
      .text().trim()
      .split(' ')[0].trim();

    const description = $('div#description span[style="display:none"]').text() || $('div#description span').text();

    const authorUrls = [];
    $('div#bookAuthors a[itemprop="url"]').each(async (i, el) => {
      const element: any = $(el);
      const authorUrl = element.attr('href');
      authorUrls.push(authorUrl);
    });

    const getAuthors = async (array) => {
      const pArray: Author [] = array.map(async authorUrl => {
        const response: Author = await this.addAuthor(authorUrl);
        return response;
      });
      const authorsArray = await Promise.all(pArray);
      return authorsArray;
    };

    const authors: Author[] = await getAuthors(authorUrls);

    const genres = [];
    $('a[class="actionLinkLite bookPageGenreLink"]').each((i, element) => {
      const genreName = $(element).text().trim();
      const genre = new Genre();
      genre.genreName = genreName;
      if (genres.length < 6) {
        genres.push(genre);
      }
    });

    const book: BookDTO = {
      title,
      pages,
      issued,
      coverUrl,
      description,
      ISBN: isbn,
      genres,
      authors,
    };
    return book;
  }

  async addAuthor(authorUrl: string) {
    const body = await this.httpService.get(authorUrl).toPromise();
    const $ = cheerio.load(body.data);

    const fullName = $('h1.authorName span[itemprop="name"]').text().trim();
    const birthDate = $('.dataItem[itemprop="birthDate"]').text().trim();
    const deathDate = $('dataItem[itemprop="deathDate"]').text().trim();
    const pictureUrl = $('img[itemprop="image"]').attr('src');
    const biography = $('div.aboutAuthorInfo span[style="display:none"]').text() || $('div.aboutAuthorInfo span').text();

    const genres: Genre[] = [];
    $('div.dataTitle:contains("Genre")').next().children('a').each((i, element) => {
      const genreName = $(element).text().trim();
      const genre = new Genre ();
      genre.genreName = genreName;

      if (genres.length < 6) {
        genres.push(genre);
      }
    });

    const author = new Author();
    author.fullName = fullName;
    author.born = birthDate;
    author.died = deathDate;
    author.biography = biography;
    author.pictureUrl = pictureUrl;
    author.genres = genres;

    return author;
  }
}
