import { JwtService } from '@nestjs/jwt';
import { GetUserDTO } from '../../models/user/get-user.dto';
import { UserLoginDTO } from '../../models/user/user-login.dto';
import { UserService } from '../user.service';
import { Injectable } from '@nestjs/common';
import { JwtPayload } from '../../interfaces/jwt-payload';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) { }

  public async signIn(user: UserLoginDTO): Promise<string> {
    const userFound: GetUserDTO = await this.userService.signIn(user);
    if (userFound) {
      return this.jwtService.sign({ username: userFound.username, isAdmin: userFound.isAdmin });
    } else {
      return null;
    }
  }

  async validateUser(payload: JwtPayload): Promise<GetUserDTO> {
    return await this.userService.validateUser(payload);
  }
}
