import { Injectable, BadRequestException } from '@nestjs/common';
import { Book } from '../database/entities/book.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { BookDTO } from '../models/book.dto';
import { AuthorsService } from './authors.service';
import { GenresService } from './genres.services';

@Injectable()
export class BooksService {
  constructor(
    @InjectRepository(Book) private readonly bookRepository: Repository<Book>,
    private readonly authorsService: AuthorsService,
    private readonly genresService: GenresService,
  ) { }

  async addBook(book: BookDTO) {
    const bookFound = await this.bookRepository.findOne({ where: { title: book.title } });
    if (bookFound) {
      throw new BadRequestException(`Book with title "${book.title}" already exists in database.`);
    }

    for (const author of book.authors) {
      const takenAuthor = await this.authorsService.getAuthor(author);
      author.id = takenAuthor.id;
    }

    for (const genre of book.genres) {
      const takenGenre = await this.genresService.getGenre(genre);
      genre.id = takenGenre.id;
    }

    const newBook = await this.bookRepository.create(book);
    await this.bookRepository.save([newBook]);

    return `The book "${book.title}" was successfully added to database.`;
  }

  async updateBookByTitle(bookTitle, bookDTO: BookDTO): Promise<string> {
    const bookToUpdate = await this.bookRepository.findOne({ where: { title: bookTitle } });

    if (!bookToUpdate) {
      throw new BadRequestException(`Book with title "${bookTitle}" does not exist in database.`);
    }
    await this.bookRepository.update(bookToUpdate, bookDTO);

    return `Book with title "${bookDTO.title}" was successfully updated.`;
  }

  async getAll(): Promise<Book[]> {
    return await this.bookRepository.find();
  }

  async getBookByTitle(bookTitle): Promise<Book> {
    const bookFound = await this.bookRepository.findOne({ where: { title: bookTitle } });

    if (!bookFound) {
      throw new BadRequestException(`Book with title "${bookTitle}" does not exist in database.`);
    }
    return bookFound;
  }

  async getBooksByFirstLetters(letter): Promise<Book[]> {
    const booksFound: Book[] = await this.bookRepository.find({ title: Like(`${letter}%`) });

    if (!booksFound) {
      throw new BadRequestException(`Sorry, no books with initial letter "${letter}" were found.`);
    }

    return booksFound;
  }
}
