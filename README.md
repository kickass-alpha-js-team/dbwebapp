# Goodbooks
## Team details
### Team name: 
- MaNi-MaNi Technologies
### Team members: 
- **Maria Dineva** (Username in telerikacademy.com - maria.dineva)
- **Nikolay Vassilev** (Username in telerikacademy.com - hldd3n)
## Gitlab repository 
- Link: https://gitlab.com/kickass-alpha-js-team/dbwebapp
## Used assets in the project
- TypeScript
- TSLint
- NestJS
- TypeORM
- MariaDB
- MySQL
- Jest
- GitLab
- Trello
### Project Information
1. Run **npm install**
2. With **npm start** you can start the server and access the routes using Postman or other API development environment. In order to run it in watch mode, you can use **npm run start:dev** command.
4. With **npm test** you can run the unit tests. With **npm run test:cov** you can check the coverage of the unit tests.
### Project Description:
Our project consists of a database and a server-side routing of a book information application. It consists of three layers:
- **Public** - It is accessible to all users, with and without authentication. These are the register and the login pages, as well as the list of all books, all authors and all genres present in the database.
- **Private part** - It is accessible only after authentication. It consists of a favorite book list of each user and user details list (first name, last name, age etc. which are all entirely optional). Each user can add and delete books from their own lists and add, edit and delete info from their details list.
- **Administrative part** - It is accessible only to authorized users, who are admins. They can add, edit and delete books and authors from the database. To help the admins with their work, we have created a scraper functionality which retrieves information about authors and books from the popular website **https://www.goodreads.com/**. It comes in JSON format so that admins can directly add it to the database.
